package com.rxtech.logmachine;

import static com.rxtech.logmachine.utils.IOUtils.*;
import static com.rxtech.logmachine.utils.JSONUtils.*;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


/**
 * Created by ������ on 14.08.2015.
 */
public class LoggerConfig {

    private PrintQueue queue = new STDOUTPrintQueue();

    public LoggerConfig() {
    }

    public LoggerConfig fromFile(File configFile) {
        String config = "";
        try {
            config = inputStreamToString(new FileInputStream(configFile), 100);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        JSONObject configObject = new JSONObject(config);
        if (!checkKeys(configObject, "type", "destination")){
            String type = configObject.getString("type");
            if (type.equals("stdout")) {
                return this;
            } else if (type.equals("http")) {
                if (!checkKeys(configObject, "method")) {
                    String url = configObject.getString("destination");
                    String method = configObject.getString("method");
                    this.queue = new HTTPPrintQueue(url, method);
                    return this;
                }
            } else if (type.equals("file")) {
                this.queue = new FilePrintQueue(new File(configObject.getString("destination")));
                return this;
            }
        }
        return this;
    }

    public LoggerConfig fromPrams(String... params){
        for (String param : params) {
            if (param.equals("stdout")) {
                break;
            } else if (param.equals("http")) {
                this.queue = new HTTPPrintQueue(params[1], params[2]);
                break;
            } else if (param.equals("file")) {
                this.queue = new FilePrintQueue(new File(params[1]));
                break;
            }
        }
        return this;
    }

    public PrintQueue getQueue() {
        return queue;
    }
}

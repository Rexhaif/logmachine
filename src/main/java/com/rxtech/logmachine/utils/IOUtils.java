package com.rxtech.logmachine.utils;

import java.io.*;

/**
 * Created by ������ on 14.08.2015.
 */
public class IOUtils {

    public static String inputStreamToString(final InputStream is, final int bufferSize) {
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        try {
            Reader in = new InputStreamReader(is, "UTF-8");
            for (;;) {
                int rsz = in.read(buffer, 0, buffer.length);
                if (rsz < 0)
                    break;
                out.append(buffer, 0, rsz);
            }
        }
        catch (UnsupportedEncodingException ex) {
        /* ... */
        }
        catch (IOException ex) {
        /* ... */
        }
        return out.toString();
    }

}

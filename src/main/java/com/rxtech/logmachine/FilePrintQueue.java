package com.rxtech.logmachine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ������ on 14.08.2015.
 */
public class FilePrintQueue implements PrintQueue {

    private Queue<String> msgPool = new ConcurrentLinkedQueue<String>();

    private File destination;

    public FilePrintQueue(final File destination) {
        this.destination = destination;
        new Thread(() -> {
            while(!msgPool.isEmpty()) {
                FileOutputStream fileOutputStream = null;
                try {
                    fileOutputStream = new FileOutputStream(destination);
                    fileOutputStream.write(msgPool.poll().getBytes(Charset.forName("UTF-8")));
                    fileOutputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (fileOutputStream != null){
                        try {
                            fileOutputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }


            }
        }).start();
    }

    public void send(String msg) {
        msgPool.add(msg);
    }
}

package com.rxtech.logmachine;


/**
 * Created by ������ on 17.08.2015.
 */
public class TestLogger {

    public static void main(String[] args) {
        final String TAG = "TEST";
        Logger.init(new LoggerConfig().fromPrams("http", "http://requestb.in/11m0ws61", "POST"));
        long t1 = System.nanoTime();
        Logger.v(TAG, "Verbose test");
        long t2 = System.nanoTime();
        System.out.println("Logger call finished in " + (t2 - t1) + " nanoseconds.");
        long t3 = System.nanoTime();
        System.out.println("Standard out finished in " + (t3 - t2) + " nanoseconds.");
    }

}

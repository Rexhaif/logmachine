package com.rxtech.logmachine;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ������ on 14.08.2015.
 */
public class STDOUTPrintQueue implements PrintQueue {

    private Queue<String> msgPool = new ConcurrentLinkedQueue<String>();


    public STDOUTPrintQueue() {
        new Thread(() -> {
            while(!msgPool.isEmpty()){
                System.out.println(msgPool.poll());
            }
        }).start();
    }


    public void send(String msg) {
        msgPool.add(msg);
    }


}

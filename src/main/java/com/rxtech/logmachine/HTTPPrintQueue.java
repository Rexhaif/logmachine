package com.rxtech.logmachine;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by ������ on 14.08.2015.
 */
public class HTTPPrintQueue implements PrintQueue {

    private Queue<String> queue = new ConcurrentLinkedQueue<String>();

    private URL url;

    private String method;

    public HTTPPrintQueue(String url, String method){
        try {
            this.url = new URL(url);
            this.method = method;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            while (!queue.isEmpty()) {
                try {
                    HttpURLConnection conn = (HttpURLConnection) this.url.openConnection();
                    conn.setRequestMethod(method);
                    conn.setDoOutput(true);
                    conn.setChunkedStreamingMode(0);
                    conn.getOutputStream().write(queue.poll().getBytes(Charset.forName("UTF-8")));
                    conn.getOutputStream().flush();
                    conn.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void send(String msg) {
        queue.add(msg);
    }
}

package com.rxtech.logmachine;

/**
 * Created by ������ on 17.08.2015.
 */
public class LogController {

    private static PrintQueue printQueue = new STDOUTPrintQueue();

    private static LogController ourInstance = new LogController();

    public static LogController getInstance(PrintQueue queue) {
        if (printQueue.equals(queue)) {
            return ourInstance;
        } else {
            printQueue = queue;
            return ourInstance;
        }
    }

    private LogController() {
    }

    public void processMessage(String message) {
        printQueue.send(message);
    }
}
